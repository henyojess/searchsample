import searchsample.*
class BootStrap {

    def init = { servletContext ->
    	System.out.println("Running bootstrap")
    	//create 5 topics
    	(1..5).each{ n ->
    		def topic = new Topic(description:"foo${n}")
    		topic.save(flush:true,failOnError:true)
    	}
    	//create an article with foo1 and foo2 topic
    	Article article = new Article(title:"bar1 title")
    	article.addToTopics(Topic.get(1))
    	article.addToTopics(Topic.get(2))
    	article.save(flush:true,failOnError:true)

    	article = new Article(title:"bar2 title")
    	article.addToTopics(Topic.get(1))
    	article.addToTopics(Topic.get(3))
    	article.save(flush:true,failOnError:true)
    }
    def destroy = {
    }
}
