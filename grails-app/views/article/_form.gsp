<%@ page import="searchsample.Article" %>



<div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="article.title.label" default="Title" />
		
	</label>
	<g:textField name="title" value="${articleInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'topics', 'error')} ">
	<label for="topics">
		<g:message code="article.topics.label" default="Topics" />
		
	</label>
	<g:select name="topics" from="${searchsample.Topic.list()}" multiple="multiple" optionKey="id" size="5" value="${articleInstance?.topics*.id}" class="many-to-many"/>
</div>

