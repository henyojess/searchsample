package searchsample

class Article {
	String title
	Set topics
    static constraints = {
    }
    static hasMany = [topics:Topic]
    static searchable = {
    	title index:'analyzed'
    	topics component:true
    }
}
