package searchsample

class Topic {
	String description
    static constraints = {
    }
    static searchable = {
    	mapping root:false
    	description index:'not_analyzed'
    }
}
